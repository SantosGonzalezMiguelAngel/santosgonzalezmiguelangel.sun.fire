<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'censos' => array(
    )
  );
  $censos = $app->db->query("select * from censo");
  foreach ($censos as $censo) {
    $datos['censos'][] = $censo;
  }

  $app->render('xml.php', $datos);
});



$app->delete('/xml/:id', function($id) use($db,$app) {
  if (id_invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  } 
	$id_cens=$id;

  $consulta="delete from censo where id_censo='".$id_cens."'"; 
  $qry = $app->db->prepare($consulta);
  if ($qry->execute() === false) {
    $app->halt(500, "Error: No se ha podido borrar lcon id '$id'.");
  }
	if ($qry->rowCount() ==0) {
    $app->halt(404, "Error: El indicador con id '$id' no existe.");
  }

  $app->halt(200, "Exito: El indicador con id '$id' ha sido borrado");
});




