<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentaci&oacute;n de API</title>
  </head>
  <body>
    <a href="../">Regresar</a>
    <hr />
    <div class="inner">
	<div class="header-meta"></div>
    <div> <h3 style="text-align: center"></a>Censos Econ&oacute;micos 2009 </h3>
      <hr align="CENTER"  width="950" color="Red" noshade>
    </div>
    <h2>Documentaci&oacute;n de API</h2>
    <ul class="list-1">
     <h2> La API se basa en los principios de REST y expone los datos de los censos econ&oacute;micos 2009 </h2>
    </ul>
    <p>&nbsp;</p>
  </div>
    
    <p>
      La URL ra&oiacute;z es <a href="http://santosgonzalezmiguelangel.sun.fire/api/">http://santosgonzalezmiguelangel.sun.fire/api/</a>.
    </p>
    <hr />

    <h2>Obtener XML correspondiente a los Datos del Censo</h2>

     Paginar los datos del censo:
Obtenga un archivo XML con una lista paginada de los datos del censo.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/censos/xml (url muestra)<br> 
*M&eacute;todo HTTP: GET<br> 
*Paginar los datos del censo por id_indicador<br>
*Obtenga un archivo XML con una lista paginada de los datos del censo.<br> 
URL: https://santosgonzalezmiguelangel.sun.fire/censos/xml/id (url muestra)<br> 
M&eacute;todo HTTP: GET  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a1.jpg" alt="">
</pre>
      </li>
    </ul>

    <h2>Obtener XML correspondiente al tema</h2>
    Obtenga un archivo XML con una lista paginada de los temas.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/tema/xml/id_tema<br>
*M&eacute;todo HTTP: GET<br>
*Paginar los datos del tema<br>
*Obtenga un archivo XML con una lista paginada de los datos del tema.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/tema/xml/id_tema<br>
M&eacute;todo HTTP: GET  clave: id_tema (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_tema</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a2.jpg" alt="">
</pre>
      </li>
    </ul>
<h2>Obtener XML correspondiente al tema sin id_tema</h2>
    Obtenga un archivo XML con una lista paginada de los temas sin el id_tema.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/tema/xml/<br>
*M&eacute;todo HTTP: GET<br>
*Paginar los datos del tema<br>
*Obtenga un archivo XML con una lista paginada de los datos del tema sin id_tema.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/tema/xml/<br>
M&eacute;todo HTTP: GET
   </div>
    <ul>
      <ul>
    
<li><strong>Presentaci&oacute;nn de respuesta</strong: XML</lli>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a3.jpg" alt="">
</pre>
      </li>
    </ul>
 <h2>Obtener XML correspondiente a los Indicadores de los censos</h2>

    Paginar los datos de los indicadores:
Obtenga un archivo XML con una lista paginada de los indicadores correspondientes a los censos.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/indicador/xml (url muestra)<br> 
*M&eacute;todo HTTP: GET<br> 
*Paginar los datos de los indicadores<br>
*Obtenga un archivo XML con una lista paginada de los indicadores correspondientes a los censos.<br> 
<li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
	  <img src="i.jpg" alt="">
	   </li>

URL: https://santosgonzalezmiguelangel.sun.fire/indicador/xml/id (url muestra)<br> 
M&eacute;todo HTTP: GET  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="i.JPG" alt="">
</pre>
	   </li>
    </ul>
<h2>Obtener XML correspondiente a las Entidades de los censos</h2>

    Paginar los datos de las entidades:
Obtenga un archivo XML con una lista paginada de las entidades correspondientes a los censos.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/entidad/xml (url muestra)<br> 
*M&eacute;todo HTTP: GET<br> 
*Paginar los datos de las entidades<br>
*Obtenga un archivo XML con una lista paginada de las entidades correspondientes a los censos.<br> 
<li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
       <ul>
	<li><strong>Ejemplo de respuesta</strong>:
	  <img src="e.jpg" alt="">
	   </li>
	</ul>
URL: https://santosgonzalezmiguelangel.sun.fire/entidad/xml/id (url muestra)<br> 
M&eacute;todo HTTP: GET  clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
	  <img src="e1.jpg" alt="">
	   </li>
    </ul>
<h2>Modificar datos correspondientes a los indicadores</h2>
    Modificar descripci&oacute;n de los indicadores:
URL: https://santosgonzalezmiguelangel.sun.fire/indicador/id (url muestra)<br> 
M&eacute;todo HTTP: POST  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta despu&eacute;s de utilizar el m&eacute;todo GET con el id del indicador</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
	  <img src="i2.jpg" alt="">
	   </li>
    </ul>
<h2>Modificar datos correspondientes a una Entidades</h2>
     Modificar descripci&oacute;n de Entidades:
URL: https://santosgonzalezmiguelangel.sun.fire/entidad/id (url muestra)<br> 
M&eacute;todo HTTP: POST  clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta despu&eacute;s de utilizar el m&eacute;todo GET con el id de la entidad</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
	  <img src="e2.jpg" alt="">
	   </li>
    </ul>
<h2>Eliminar datos correspondientes a las Entidades de los censos</h2>
    Eliminar los datos de entidades:
Elimine los datos de una entidad que corresponda a un id.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/entidad/id (url muestra)<br> 
M&eacute;todo HTTP: DELETE clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      

    </ul>
<h2>Eliminar datos correspondientes a los indicadores de los censos</h2>
    Eliminar los datos de los indicadores:
Elimine los datos de un indicador que corresponda a un id.<br>
URL: https://santosgonzalezmiguelangel.sun.fire/indicador/id (url muestra)<br> 
M&eacute;todo HTTP: DELETE clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par&aacute;metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci&oacute;n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
	  <img src="i3.jpg" alt="">
	   </li>
    </ul>      
  </body>
</html>
